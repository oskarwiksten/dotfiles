#!/bin/sh

set -ex

v=$1
if [ -z "$v" ] ; then
	echo 'Supply target version as $1'
	exit 1
fi

mvn versions:set -DnewVersion=$v -DgenerateBackupPoms=false -DprocessAllModules=true

