#!/bin/sh

for b in $( git-list-remote-tracking-pr-branches.sh ) ; do 
	git co -q ${b}
	git status -sb
	read -p 'push -f [y/N] ? ' yn
	if [ "${yn}" = "y" ] ; then 
		git push -f
	fi
done

git co master

