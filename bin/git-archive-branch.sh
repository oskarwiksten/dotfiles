#!/bin/sh

_branch=$1
_base=$(git merge-base master ${_branch})
mkdir -p "${_branch}/${_base}"
(
cd "${_branch}/${_base}"
git format-patch ${_base}...${_branch}
)
