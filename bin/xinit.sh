#!/bin/sh

pgrep --uid "$USER" xbindkeys >/dev/null || xbindkeys &

/usr/bin/xset m 2/1 1

#disable dpms & screensaver
/usr/bin/xset -dpms
/usr/bin/xset s off
# Disable bell
/usr/bin/xset b off

