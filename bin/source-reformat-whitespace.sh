#/bin/sh

find . -iname '*.java' -print0 | xargs -0 dos2unix 
for i in $(find . -iname '*.java') ; do iconv --from cp1252 --to utf-8 "$i" >tmp ; mv tmp "$i" ; done
find . -iname '*.java' -print0 | xargs -0 sed -r -i -e 's/\s+$//g'
find . -iname '*.java' -print0 | xargs -0 sed -r -i -e 's/    /\t/g'
