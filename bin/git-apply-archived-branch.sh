#!/bin/sh

# expects files like this:
#   oskar@oskardebian:~/projects/izettle-planet$ ls -l xac-audio/apply-branch-test/7efe2fdb58113d942993792b554e5ae36de470f5/
#   total 172
#   -rw-r--r-- 1 oskar oskar 46989 May 26 09:18 0001-XAC-Audio-Even-more-strict-amplitude-checking-when-l.patch
#   -rw-r--r-- 1 oskar oskar 90493 May 26 09:18 0002-Add-failing-unit-tests-for-gating.patch
#   -rw-r--r-- 1 oskar oskar 13781 May 26 09:18 0003-Add-failing-unit-tests-for-parsing-noise-as-empty.patch
#   -rw-r--r-- 1 oskar oskar  2079 May 26 09:18 0004-add-debug-info.patch
#   -rw-r--r-- 1 oskar oskar  7776 May 26 09:18 0005-TEST-TEST-TEST-flush-intermediate-sample-steps-to-di.patch
#   -rw-r--r-- 1 oskar oskar  3669 May 26 09:18 0006-Make-parser-tests-work.patch

_branch=${1%/}
if [ -z ${_branch} ] ; then
	echo "Usage $0 [dir with branch]"
	exit 1
fi
if [ ! -d ${_branch} ] ; then
	echo "Cannot find directory ${_branch}"
	exit 1
fi

_base=$(basename ${_branch}/*/)
if [ ! -f ${_branch}/${_base}/0001-*.patch ] ; then
	echo "Could not find any ${_branch}/${_base}/0001-*.patch file."
	exit 1
fi

if [ -n "$( git branch | fgrep ${_branch} )" ] ; then
	echo "git branch ${_branch} already exists, aborting."
	exit 1
fi

git co master
git co -b ${_branch}
git reset --hard ${_base}
git am -3 --ignore-whitespace ${_branch}/${_base}/*.patch
