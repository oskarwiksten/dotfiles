#!/bin/sh

xterm -fg white -bg black -fn 7x13 -fa "monospace:size=20:antialias=true" -sl 1000 -vb +ls "$@" &
