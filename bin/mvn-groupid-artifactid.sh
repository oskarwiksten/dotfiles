#!/bin/sh

xml sel -N ns=http://maven.apache.org/POM/4.0.0 -t -m 'ns:project' -v 'ns:groupId' -o : -v 'ns:artifactId' -o : -m 'ns:parent' -v 'ns:groupId' \
	| sed -E -e 's,^:(.*):(.*),\2:\1,; s,(.*):(.*):(.*),\1:\2,'

