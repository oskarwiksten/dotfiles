#!/bin/sh

exec xterm \
	-u8 \
	-geometry 80x25 \
	-sl 1000 \
	+vb \
	-ls \
	"$@"
