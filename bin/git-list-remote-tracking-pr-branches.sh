#!/bin/sh

git branch -vv \
	| tr -d '*' \
	| awk '{print $3}' \
	| fgrep '[origin/' \
	| tr -d '[]:' \
	| egrep -v '^origin/(production|staging|master)' \
	| sed -e 's,^origin/,,'
