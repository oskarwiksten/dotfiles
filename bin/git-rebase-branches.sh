#!/bin/sh

while read branch ; do
	echo
	echo "========== ${branch}"
	git checkout "${branch}"
	git rebase master || exit
done

echo
echo "========== master"
git checkout master
