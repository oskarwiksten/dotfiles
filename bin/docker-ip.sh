#!/bin/sh

_container="$1"
test -z "$_container" && echo "Must specify container name" && exit 1
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' "$_container"
