#!/bin/sh

for branch in $( git branch -r | egrep -v '/(HEAD|master)' ) ; do git show --format="%an    %ai    %ar    $branch" $branch | head -n 1 ; done | egrep -v '(days|hours) ago' | sort
