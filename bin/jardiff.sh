#!/bin/bash

set -e

f1="$1"
f2="$2"
d1=$( mktemp -d )
(cd "$d1"; jar xf "$f1")

d2=$( mktemp -d )
(cd "$d2"; jar xf "$f2")

(
cd "$d1"
for i in $( find . -name '*.class') ; do
	diff -Naurw <( javap "$i" ) <( javap "$d2"/"$i" ) || true
done
)

rm -fR "$d1" "$d2"

