#!/bin/sh

xml sel -N ns=http://maven.apache.org/POM/4.0.0 -t -m 'ns:project' -v 'ns:groupId' -o : -m 'ns:parent' -v 'ns:groupId' <pom.xml \
	| sed -e 's,^:,,; s,:.*,,'

