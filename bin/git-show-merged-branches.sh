#!/bin/sh

for k in $(git branch -r --merged|grep -v "\->"|sed s/^..//);do echo -e $(git log -1 --pretty=format:"%Cgreen%ci %Cred%cr %Cblue%ce%Creset" "$k")\\t"$k";done|sort
