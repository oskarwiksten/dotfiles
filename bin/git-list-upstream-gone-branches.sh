#!/bin/sh

git branch --list --format '%(refname:lstrip=2) %(upstream)' \
  | fgrep refs \
  | while read L R ; do
    git branch --remotes --format '%(refname)' \
    | fgrep -q --line-regex --fixed-strings "$R" \
    || echo "$L"
done

