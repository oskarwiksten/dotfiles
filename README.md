# dotfiles

My GNU/Linux dotfiles and various shellscripts

Heavily based on ["Cowboy" Ben Alman 's "dotfiles" repo](https://github.com/cowboy/dotfiles/).

## Installation

```sh
git clone https://gitlab.com/oskarwiksten/dotfiles.git ~/.dotfiles
~/.dotfiles/bin/dotfiles
```

## What, exactly, does the "dotfiles" command do?

It's really not very complicated. When [bin/dotfiles](bin/dotfiles) is run, it does a few things:

2. Files in `init` are executed (in alphanumeric order, hence the "50_" names).
3. Files in `copy` are copied into `~/`.
4. Files in `link` are linked into `~/`.

### Directories

dir | description
----|:------------
[`bin`](bin/) | Will be added to `$PATH`. Executable shell scripts.
[`conf`](conf/) | Files in just sit there. If a config file doesn't _need_ to go in `~/`, put it in there.
[`copy`](copy/) | Copied into `~/`. Any file that _needs_ to be modified with personal information (like [.gitconfig](copy/.gitconfig) which contains an email address and private key) should be _copied_ into `~/`. Because the file you'll be editing is no longer in `~/.dotfiles`, it's less likely to be accidentally committed into your public dotfiles repo.
[`init`](init/) | Executed in order when installing dotfiles.
[`libs`](libs/) | Git submodules of references libraries
[`link`](link/) | Files gets symbolically linked with `ln -s` into `~/`. Edit these, and you change the file in the repo. Don't link files containing sensitive data, or you might accidentally commit that data!
[`source`](source/) | Files in here get sourced/executed whenever a new shell is opened (in alphanumeric order, hence the "50_" names).
`backups` | only gets created when necessary. Any files in `~/` that would have been overwritten by `copy` or `link` get backed up there.
`caches` | cached files, only used by some scripts. This folder will only be created if necessary.

### Aliases and Functions
To keep things easy, the `~/.bashrc` and `~/.bash_profile` files are extremely simple, and should never need to be modified. Instead, add your aliases, functions, settings, etc into one of the files in the `source` subdirectory, or add a new file. They're all automatically sourced when a new shell is opened. Take a look, I have [a lot of aliases and functions](source).

### Scripts
In addition to the aforementioned [dotfiles](bin/dotfiles) script, there are a few other [bash scripts](bin/).

* [dotfiles](bin/dotfiles) - (re)initialize dotfiles.
* [src](link/.bashrc#L6-15) - (re)source all files in `source` directory
* Look through the [bin](bin/) subdirectory for a few more.

## Inspiration
* <https://github.com/cowboy/dotfiles/>  Last updated from commit 42e912e4939a3c1fc688ab6c37fc32cf851dc066  
* (and 10+ years of accumulated crap)

## License
Copyright (c) 2013 "Cowboy" Ben Alman  
Copyright (c) 2013-2014 Oskar Wiksten
Licensed under the MIT license.  
