# Send xterm keys (ctrl-arrow)
set-window-option -g xterm-keys on

# New windows should have correct $TERM (even when .bashrc is not run)
set -g default-terminal "screen-256color"
# enable RGB capability for neovim
set-option -sa terminal-overrides ',xterm:RGB'

# Set the base index for windows to 1 instead of 0.
set -g base-index 1
# Set the base index for panes to 1 instead of 0.
setw -g pane-base-index 1

# alt+left/right cycles thru windows
bind-key -n M-right next
bind-key -n M-left prev

# Colors
set -g status-fg white
set -g status-bg black
set-window-option -g window-status-style bg=colour22
set-window-option -g window-status-current-style bg=colour88

# Splitting panes.
bind | split-window -h
bind - split-window -v

# Window titles
set -g set-titles on

# Disable status-right. Default is: status-right ""#{=22:pane_title}" %H:%M %d-%b-%y"
set -g status-right ''

# Don't capture escape
set -s escape-time 0

# Scroll buffer size
set-option -g history-limit 5000

# enable focus events for neovim `autoread`
set-option -g focus-events on

# vi keybindings and copy-paste
set-window-option -g mode-keys vi
bind-key -T copy-mode-vi 'v' send -X begin-selection
bind-key -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -in -filter -selection primary | xclip -in -selection clipboard"

# prefix-Enter enters copy mode
bind-key -T prefix Enter copy-mode

# In copy-mode-vi, 'y' will copy whole line
bind-key -r -T copy-mode-vi y send-keys -X back-to-indentation \; send-keys -X begin-selection \; send-keys -X end-of-line \; send-keys -X cursor-left \; send-keys -X copy-pipe-and-cancel "xclip -in -filter -selection primary | xclip -in -selection clipboard"

# when closing last window, do not detach - instead go to last active session
set-option -g detach-on-destroy off

# shift-P shows the contents of the current pane in a scite editor
bind-key P capture-pane -J -S -50000 \; save-buffer /tmp/oskar.tmux-buffer.log \; delete-buffer \; new-window -n buffer 'vim /tmp/oskar.tmux-buffer.log'
# shift-R repacks window numbers in sequential order
bind-key R move-window -r
# shift-C opens new window in current dir
bind-key C new-window -c "#{pane_current_path}"

