syntax on
colorscheme termschool
set tabstop=4
set shiftwidth=4
set ignorecase
set background=dark
set encoding=utf8
set relativenumber
set number
set modelines=0
set nomodeline
set scrolloff=15
set laststatus=2
set autoread

" Allow backspace to remove indents, eols and text outside current edit
set backspace=indent,eol,start

" highlight current line
set cul

" Disable bells
set noerrorbells visualbell t_vb=

if &term =~ '^screen'
    " tmux will send xterm-style keys when xterm-keys is on
    execute "set <xUp>=\e[1;*A"
    execute "set <xDown>=\e[1;*B"
    execute "set <xRight>=\e[1;*C"
    execute "set <xLeft>=\e[1;*D"
endif

" Format *.cnf files same as dos ini when reading a buffer and when opening a new buffer
autocmd BufRead,BufNewFile *.conf set filetype=dosini
autocmd BufRead,BufNewFile Jenkinsfile* set filetype=groovy
autocmd FileType gitcommit setlocal tw=72 colorcolumn=72
