theme = dofile("/usr/share/awesome/themes/default/theme.lua")

theme.wallpaper = "/usr/share/images/desktop-base/default"
theme.border_width  = 0

return theme
